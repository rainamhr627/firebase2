package com.example.raina.firebase.interfaces;

import com.example.raina.firebase.model.User;

/**
 * Created by Admin on 7/3/2017.
 */

public interface OnLongPress {

    void OnLongClick(User user);
}
