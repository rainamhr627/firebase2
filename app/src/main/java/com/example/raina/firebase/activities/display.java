package com.example.raina.firebase.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.raina.firebase.R;
import com.example.raina.firebase.adapter.Recyclerview_adapter;
import com.example.raina.firebase.interfaces.OnLongPress;
import com.example.raina.firebase.model.Id;
import com.example.raina.firebase.model.User;
import com.google.android.gms.common.UserRecoverableException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.raina.firebase.R.id.save;
import static com.example.raina.firebase.model.Id.userId;

/**
 * Created by Admin on 6/25/2017.
 */

public class display extends AppCompatActivity {
    final String TAG = getClass().getSimpleName();

    RecyclerView recyclerView;
    Recyclerview_adapter rec_adapter;
    public DatabaseReference mFirebaseDatabase;

    private static final int PENDING_REMOVAL_TIMEOUT = 3000;

    boolean undoOn;

    List<User> userlist = new ArrayList<User>();

    private FloatingActionButton floatingActionButton;
    Dialog dialog;
    Button saved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(SAVECLICK);
    }

     EditText inputName1, inputEmail1, inputNumber1;

     private View.OnClickListener SAVECLICK = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.e("floatingButton", "entered");
            dialog = new Dialog(display.this);
            dialog.setContentView(R.layout.add_member_dialog);
            dialog.setTitle("ADD MEMBER");

            saved = (Button) dialog.findViewById(R.id.saved);

            inputName1 = (EditText) dialog.findViewById(R.id.name1);
            inputEmail1 = (EditText) dialog.findViewById(R.id.email1);
            inputNumber1 = (EditText) dialog.findViewById(R.id.number1);

            try {
                saved.setOnClickListener(SAVE);
            } catch (Exception e) {
                Log.e("ERROR...", e.getMessage());

            }
            dialog.show();

        }
     };

    private View.OnClickListener SAVE = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            Log.d("saveddddd", "triggered");

            User user = new User();

            user.setName(inputName1.getText().toString());
            user.setEmail(inputEmail1.getText().toString());
            user.setNumber(inputNumber1.getText().toString());

            DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("users").child(Id.userId);

            String id =mFirebaseDatabase.push().getKey();

            dataRef.child(id).setValue(user);
            dialog.dismiss();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.e("menu-create", "asd");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("menuuuu", "abvnn");
        if (item.getItemId() == R.id.menu_item_undo_checkbox) {

            Log.e("menuuuu", "abv");

            item.setChecked(!item.isChecked());
            ((Recyclerview_adapter) recyclerView.getAdapter()).setUndoOn(item.isChecked());
        }
        if (item.getItemId() == R.id.menu_item_add_5_items) {
            ((Recyclerview_adapter) recyclerView.getAdapter()).setUndoOn(item.isChecked());
        }
        return super.onOptionsItemSelected(item);
    }


    public boolean isUndoOn() {
        return undoOn;
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();

        mFirebaseDatabase = mFirebaseInstance.getReference("users");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(userId);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("mData", dataSnapshot.getKey());
                Log.d("mData", String.valueOf(dataSnapshot.hasChildren()));
                Log.d("mData", String.valueOf(dataSnapshot.getChildrenCount()));

                userlist.clear();

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    Log.d("mData Key", userSnapshot.getKey());

                    Log.d("mdata", userSnapshot.toString());

                    User _user = userSnapshot.getValue(User.class);
                    User user = new User(_user, userSnapshot.getKey());
                    // User user = userSnapshot.getValue(User.class);
                    Log.d("mData idvalue", user.toString());
                    userlist.add(user);
                    Log.d("mData User", user.getEmail());
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(getApplication()));

                rec_adapter = new Recyclerview_adapter(userlist, onLongPress);
                recyclerView.setAdapter(rec_adapter);
                recyclerView.setHasFixedSize(true);
                setUpItemTouchHelper();
                setUpAnimationDecoratorHelper();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    OnLongPress onLongPress = new OnLongPress() {
        @Override
        public void OnLongClick(User user) {
            Toast.makeText(getApplicationContext(), "pressed", Toast.LENGTH_LONG).show();
            updateDialog(user);
        }
    };

    public void updateDialog(final User user) {
        final Dialog update_dialog = new Dialog(this);
        View view = update_dialog.getLayoutInflater().from(this).inflate(R.layout.update_user, null);


        String title = (user != null) ? "Edit User" : "Add User";


        update_dialog.setTitle(title);

        final EditText editName, editEmail, editNumber;
        Button save;

        editName = (EditText) view.findViewById(R.id.name);
        editEmail = (EditText) view.findViewById(R.id.address);
        editNumber = (EditText) view.findViewById(R.id.number);

        editName.setText(user.getName());
        editEmail.setText(user.getEmail());
        editNumber.setText(user.getNumber());

        save = (Button) view.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("update-click", "triggered");

                DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("users").child(Id.userId);
                DatabaseReference userRef = dataRef.child(user.getKey());

                userRef.child("name").setValue(editName.getText().toString());
                userRef.child("email").setValue(editEmail.getText().toString());
                userRef.child("number").setValue(editNumber.getText().toString());

                update_dialog.dismiss();
            }
        });

        update_dialog.setContentView(view);

        update_dialog.show();
    }

    public void deleteFirebase(String key) {

        //delete file

        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference("users").child(Id.userId).child(key);
        Log.d("mdata", ref1.toString());
        // ref1.removeValue();
        ref1.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(display.this, "Sucessfully deleted",
                                        Toast.LENGTH_LONG).show();
                                // File deleted successfully
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Toast.makeText(display.this, "Error occured while deleting file",
                                        Toast.LENGTH_LONG).show();
                                // Uh-oh, an error occurred!
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("TodoApp", "getUser:onCancelled", databaseError.toException());

                    }
                });
    }

    public void pendingRemoval(int position) {
        final User item = rec_adapter.list.get(position);
        if (!rec_adapter.itemsPendingRemoval.contains(userlist)) {
            rec_adapter.itemsPendingRemoval.add((User) userlist);
            // this will redraw row in "undo" state
            rec_adapter.notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(userlist.indexOf(item));
                }
            };
            rec_adapter.handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            rec_adapter.pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }

    public void remove(int position) {
        User item = userlist.get(position);
        if (rec_adapter.itemsPendingRemoval.contains(item)) {
            rec_adapter.itemsPendingRemoval.remove(item);
        }
        if (userlist.contains(item)) {
            userlist.remove(position);
            rec_adapter.notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        User item = userlist.get(position);
        return rec_adapter.itemsPendingRemoval.contains(item);
    }

    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(display.this, R.drawable.ic_delete_black_24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) display.this.getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();

                // Recyclerview_adapter testAdapter = (Recyclerview_adapter) recyclerView.getAdapter();
                if (isUndoOn() && isPendingRemoval(position)) {
                    //  return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                String key = userlist.get(swipedPosition).getKey();
                deleteFirebase(key);
                // rec_adapter = (Recyclerview_adapter) recyclerView.getAdapter();
                boolean undoOn = isUndoOn();
                if (undoOn) {
                    pendingRemoval(swipedPosition);
                } else {
                    /*rec_adapter.remove(swipedPosition);*/
                    remove(swipedPosition);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void setUpAnimationDecoratorHelper() {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

                if (!initiated) {
                    init();
                }

                // only if animation is in progress
                if (parent.getItemAnimator().isRunning()) {

                    // some items might be animating down and some items might be animating up to close the gap left by the removed item
                    // this is not exclusive, both movement can be happening at the same time
                    // to reproduce this leave just enough items so the first one and the last one would be just a little off screen
                    // then remove one from the middle

                    // find first child with translationY > 0
                    // and last one with translationY < 0
                    // we're after a rect that is not covered in recycler-view views at this point in time
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    // this is fixed
                    int left = 0;
                    int right = parent.getWidth();

                    // this we need to find out
                    int top = 0;
                    int bottom = 0;

                    // find relevant translating views
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            // view is coming down
                            lastViewComingDown = child;
                        } else if (child.getTranslationY() > 0) {
                            // view is coming up
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        // views are coming down AND going up to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                        // views are going down to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                        // views are coming up to fill the void
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);
                }
                super.onDraw(c, parent, state);
            }

        });
    }
}








