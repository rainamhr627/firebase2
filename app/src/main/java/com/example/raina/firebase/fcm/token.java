package com.example.raina.firebase.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.raina.firebase.R;
import com.github.siyamed.shapeimageview.StarImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by queen on 7/17/17.
 */

public class token extends AppCompatActivity {

    //private TextView textView;
    //private BroadcastReceiver broadcastReceiver;
    private EditText textEmail;
    private Button btn_register;
    //private static final String URL_TOKEN = "http://192.168.1.209/storeToken.php";

    private static final String URL_TOKEN= "http:/10.79.36.143/archive/storeToken.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.token);

        textEmail = (EditText) findViewById(R.id.textEmail);
        btn_register = (Button) findViewById(R.id.btn_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("button token","clicked");
                sendTokenToServer();
            }
        });

       /* broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };
*/
       // registerReceiver(broadcastReceiver,new IntentFilter(MyFirebaseInstanceIDService.TOKEN_BROADCAST));
       /* textView = (TextView) findViewById(R.id.viewToken);
        String token = sharedPref.getInstance(this).getToken();
        textView.setText(token);*/

        Log.d("fcmToken",sharedPref.getInstance(this).getToken());
    }

    private void sendTokenToServer(){
        final String email = textEmail.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            Toast.makeText(this, "Enter your email please",Toast.LENGTH_LONG).show();
        }
        else{
            if(sharedPref.getInstance(this).getToken() !=null) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL_TOKEN,
                        new Response.Listener<String>() {
                            @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject obj = new JSONObject(response);
                                        Toast.makeText(getApplicationContext(), obj.getString("message"),Toast.LENGTH_LONG).show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("errorrr","token not sent");
                                Toast.makeText(getApplicationContext(), error.getMessage(),Toast.LENGTH_LONG).show();

                            }
                        }){

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("token", sharedPref.getInstance(getApplicationContext()).getToken());
                        params.put("email", email);
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }else{
                Toast.makeText(this, "Token not generated", Toast.LENGTH_LONG).show();
            }
        }
    }
}
