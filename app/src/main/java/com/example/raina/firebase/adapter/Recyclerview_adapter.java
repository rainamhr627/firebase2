package com.example.raina.firebase.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.os.Handler;

import com.example.raina.firebase.R;
import com.example.raina.firebase.interfaces.OnLongPress;
import com.example.raina.firebase.model.User;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 6/7/2017.
 */

public class Recyclerview_adapter extends RecyclerView.Adapter<Recyclerview_adapter.viewholder> {

    boolean undoOn;

    public List<User> list = new ArrayList<>();
    Context activity;
    OnLongPress longClickListener;

    public List<User> itemsPendingRemoval  = new ArrayList<>();

    public Handler handler = new Handler(); // hanlder for running delayed runnables
    public HashMap<User, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

    public Recyclerview_adapter(List<User> list, OnLongPress longClickListener) {
        this.list = list;
        this.longClickListener =  longClickListener;

        //this.activity = activity;
    }
    
    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_display_users,parent,false);
        return new viewholder(view);
    }

    @Override
    public void onBindViewHolder(final viewholder holder, int position) {
        Log.d("adapter", String.valueOf(position));
        final User pos = list.get(position);
        Log.d("adapter", String.valueOf(pos));
        Log.e("username", pos.getName());

        if (itemsPendingRemoval.contains(list)) {
            // we need to show the "undo" state of the row
            holder.itemView.setBackgroundColor(Color.RED);
            holder.user_name.setVisibility(View.VISIBLE);
            holder.user_email.setVisibility(View.VISIBLE);
            holder.user_number.setVisibility(View.VISIBLE);
            holder.undoButton.setVisibility(View.VISIBLE);
            holder.undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(list);
                    pendingRunnables.remove(list);
                    if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(list);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(list.indexOf(list));
                }
            });

        } else {
            // we need to show the "normal" state
            holder.itemView.setBackgroundColor(Color.WHITE);

            holder.user_name.setVisibility(View.VISIBLE);
            holder.user_email.setVisibility(View.VISIBLE);
            holder.user_number.setVisibility(View.VISIBLE);

            holder.user_name.setText(pos.getName());
            holder.user_email.setText(pos.getEmail());
            holder.user_number.setText(pos.getNumber());

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClickListener.OnLongClick(pos);
                    return true;
                }
            });

            //holder.titleTextView.setText(list);
            holder.undoButton.setVisibility(View.GONE);
            holder.undoButton.setOnClickListener((View.OnClickListener) activity);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    public class viewholder extends RecyclerView.ViewHolder{
        TextView user_name, user_email, user_number;
        Button undoButton;

        public viewholder(View itemView) {
            super(itemView);

            user_name = itemView.findViewById(R.id.txt_name);
            user_email = (TextView) itemView.findViewById(R.id.txt_email);
            user_number = itemView.findViewById(R.id.txt_number);
            undoButton = (Button) itemView.findViewById(R.id.undo_button);

        }

        /*public void bind(final User user, final OnLongPress longClickListener){
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClickListener.OnLongClick(user);
                    return true;
                }
            });

        }*/
    }

}
