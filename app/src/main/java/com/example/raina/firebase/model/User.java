package com.example.raina.firebase.model;

import com.bumptech.glide.load.model.StringLoader;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Admin on 6/23/2017.
 */

@IgnoreExtraProperties

public class User {

    String uid;
    String name;
    String email;
    String number;
    String added_by;

    private String key;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {

        return key;
    }

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    public User(String uid, String name, String email, String number, String key) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.number=number;
        this.key = key;

        //this.added_by= added_by;


    }
    public User(User user,String key) {
        this.uid = user.getUid();
        this.name = user.getName();
        this.email = user.getEmail();
        this.number=user.getNumber();
        this.key = key;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber() {
        return number;
    }




    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumber(String number) {
        this.number = number;
    }

   /* public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }*/



}
