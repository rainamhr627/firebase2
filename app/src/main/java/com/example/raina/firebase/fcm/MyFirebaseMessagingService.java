package com.example.raina.firebase.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.example.raina.firebase.R;
import com.example.raina.firebase.activities.MainActivity;
import com.example.raina.firebase.activities.home;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import static android.R.attr.data;
import static android.R.id.message;
import static com.example.raina.firebase.R.attr.title;

/**
 * Created by queen on 7/12/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String PUSH_NOTIFICATION = "pushNotification";

    String TAG = this.getClass().getSimpleName();

    private Context mContext;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("message");

            String title = data.getString("title");
            String message = data.getString("message");
            String type = data.getString("type");
            JSONObject payload = data.getJSONObject("payload");
            String videoURL = (String) payload.get  ("url");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "type: " + type);
            Log.e(TAG, "videoURL: " + videoURL);
            Log.e(TAG, "payload: " + payload.get("url"));

            SharedPreferences.Editor saveData = getSharedPreferences("data payload", MODE_PRIVATE).edit();
            saveData.putString("type", type);
            saveData.putString("videoURL", videoURL);
            saveData.commit();
            saveData.clear();

            handleNotification(data);

        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void handleNotification(JSONObject data) {
        notification(data);

        Intent pushNotification = new Intent(PUSH_NOTIFICATION);
        pushNotification.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        playNotificationSound();

    }

    public void notification(JSONObject data) {

        try {
            Log.d("notification", "sentttt");
            Intent intent = new Intent(this, home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notficationBuilder = new NotificationCompat.Builder(this);
            notficationBuilder.setContentTitle(data.getString("title"));
            notficationBuilder.setContentText(data.getString("message"));
            notficationBuilder.setAutoCancel(true);
            notficationBuilder.setSmallIcon(R.drawable.firebase);
            notficationBuilder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notficationBuilder.build());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Playing notification sound
        public void playNotificationSound() {

            Log.d("notification sound", "entered");

            try {
                Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + mContext.getPackageName() + "/raw/notification");
                Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}
