package com.example.raina.firebase.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.raina.firebase.R;
import com.example.raina.firebase.fcm.token;
import com.example.raina.firebase.model.Id;
import com.example.raina.firebase.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.security.AccessController.getContext;

public class home extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    public EditText inputName, inputEmail, inputNumber;
    public Button btnSave, btnRetrive, btnsignout, btnToken;
    public DatabaseReference mFirebaseDatabase;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;

    Id userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnToken = (Button) findViewById(R.id.btn_token);

        //camera();
        playVideo();
        btnToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home.this, token.class);
                startActivity(intent);
            }
        });

        inputName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputNumber = (EditText) findViewById(R.id.number);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnRetrive = (Button) findViewById(R.id.btn_display);
        btnsignout = (Button) findViewById(R.id.signoutbtn);


        // get reference to 'users' node

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("users");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();
            }
        });

        btnRetrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home.this, display.class);
                startActivity(intent);
            }
        });

        btnsignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    // Creating new user node under 'users'

    public void createUser() {

        String name = inputName.getText().toString();
        String email = inputEmail.getText().toString();
        String number = inputNumber.getText().toString();

        if ((!TextUtils.isEmpty(name)) && (!TextUtils.isEmpty(email)) && (!TextUtils.isEmpty(number))) {

            Toast.makeText(getApplicationContext(), "Blank field", Toast.LENGTH_LONG).show();

            String id = mFirebaseDatabase.push().getKey();

            String uid = userId.userId;

            User user = new User(uid, name, email, number, id);

            mFirebaseDatabase.child(uid).child(id).setValue(user);
            Toast.makeText(this, "Data Added!", Toast.LENGTH_LONG).show();
        }

        // clear edit text
        inputEmail.setText("");
        inputName.setText("");
        inputNumber.setText("");
    }

    public void logout() {
        Log.d("signout", "home");
        Intent logout = new Intent(home.this, MainActivity.class);
        logout.putExtra("logout1", true);
        startActivity(logout);
        finish();
    }

    public void playVideo() {
        SharedPreferences retrive = getSharedPreferences("datapayload", MODE_PRIVATE);
        String type = retrive.getString("type", "undefined type");
        String videoURL = retrive.getString("videoURL", "no url found");

        if (type.equalsIgnoreCase("video")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoURL)));
        }
    }

   /* public void camera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        imageUri = Uri.fromFile(photo);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = imageUri;
                    getContentResolver().notifyChange(selectedImage, null);
                    ImageView imageView = (ImageView) findViewById(R.id.starimg);
                    ContentResolver cr = getContentResolver();
                    Bitmap bitmap;
                    try {
                        bitmap = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, selectedImage);

                        imageView.setImageBitmap(bitmap);
                        Toast.makeText(this, selectedImage.toString(),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }*/

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmation");
        builder.setMessage("Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();

    }
}
